using System;
using System.Collections.Generic;
using SocketIOClient;
using SocketIOClient.Newtonsoft.Json;
using UnityEngine;
using UnityEngine.UI;
using Newtonsoft.Json.Linq;

public class SocketClient : MonoBehaviour
{
    public SocketIOUnity socket;
    public string serverUrlLink = "http://localhost:3000";
    void Start()
    {
        Debug.Log("socket.OnConnected");
        var uri = new Uri(serverUrlLink);       
        socket = new SocketIOUnity(uri, new SocketIOOptions{
					EIO = 3
				}); 
        socket.OnConnected += (sender, e) =>
        {
            Debug.Log("socket.OnConnected");
        };

        socket.On("unity_message", response =>
        {
            Debug.Log("Event" + response.ToString());
            Debug.Log(response.GetValue<string>());
        });

        socket.Connect();
    }

    
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            socket.EmitAsync("unity_message", "Hello, server!");
        }
        
    }
    void OnDestroy()
    {
        socket.Dispose();
    }
}